#!/usr/bin/env python3
################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os

from awsmp.constants import Proc
import awsmp.database
from awsmp.plot import plot_all_raw_pnts
from attach_fc import attach_fc
import generate_geojson
import plotting
from pnt2caaml import pnt2caaml
from pnt2meta import pnt2meta
from pnt2sp import pnt2sp
from snowt import assimilate_temps
import snowmicropyn

def batch_process_db(reprocess: bool=False, database_name: str="smp",
        outfolder=None, meteo_source=None, verbose=False):

    datab = awsmp.database.SMPDatabase()

    query = {"metadata.type": "pnt"}
    for file in datab.gfs.find(query):
        if verbose:
            print(f"[i] Working on {file.filename}")
        if reprocess:
            datab.db.fs.files.update_one(
                {"_id": file._id},
                {"$set": {"metadata.proclevel": Proc.UNPROCESSED}},
            )
        content = file.read()

        # step 1: extract metadata from pnt and put in database:
        pnt = snowmicropyn.Profile.load(content, binary=True)
        success = pnt2meta(datab, file._id, pnt)
        if not success:
            raise ValueError(f"[E] Something wrong with metadata of {file.filename}")

        # step 2: produce CAAML from pnt through SnowMicroPyn:
        success = pnt2caaml(datab, file._id, pnt)
        if not success:
            raise ValueError (f"[E] CAAML creation has failed for {file.filename}.")

        # step 3: assimilate temperatures into CAAML:
        success = assimilate_temps(datab, file._id, pnt)
        if not success:
            raise ValueError (f"[E] No temerature data could be assimilated for {file.filename}.")

        # step 4: attach meteo data for forecast:
        if meteo_source:
            success = attach_fc(datab, file._id, meteo_source, pnt, reprocess)
            if not success:
                print(f"[W] No forecast available for {file.filename}.")

        # step 5: run snowpack model:
        success = pnt2sp(datab, file._id)
        if not success:
            print(f"[W] Could not run SNOWPACK on {file.filename}.")

        # step 6: plot results:
        if outfolder:
            plot_all_raw_pnts(os.path.join(outfolder, "public/smp-chain/raw"))
            success = plotting.plot(datab, file._id, os.path.join(outfolder, "public/smp-chain/pro"))
            if not success:
                print(f"[W] Could not run snowpro on {file.filename}.")

       # step 7: update dashboard:
        if outfolder:
            generate_geojson.generate_geojson(fpublic)
            generate_geojson.update_dash_config(fpublic)

if __name__ == "__main__":
    reprocess = False
    fpublic = "/var/www/html"
    meteo_source = "arome-tirol" # TODO: from domain xml
    batch_process_db(reprocess=reprocess, outfolder=fpublic,
        meteo_source=meteo_source, verbose=True)
