################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awio
from awsmp.constants import Proc
from snowpacktools.snowpro.pro_plotter import plot_snp_evo

import configparser
import datetime
import os

def plot(datab, fid, outfolder):
    file = datab.gfs.find_one({"_id": fid})
    if file.metadata["proclevel"] >= Proc.PLOT:
        return True

    query = {"metadata.type": "smet", "metadata.pnt": f"{file.metadata['name']}.pnt"}
    smetfile = datab.gfs.find_one(query)
    edate = datetime.datetime.strptime(smetfile.metadata["period"][1], "%Y-%m-%dT%H:%M:%S")

    cfg = _snowpro_config(file, edate, outfolder)
    plot_snp_evo(cfg)

    result = datab.db.fs.files.update_one(
        {"_id": fid},
        {"$set": {
            "metadata.proclevel": Proc.PLOT,
        }},
    )
    if result.modified_count != 1:
        return False
    return True

def _snowpro_config(file, edate, outfolder):
    cfgpath = os.path.join(awio.get_scriptpath(__file__), "snowpro.ini")
    cfg = configparser.ConfigParser()
    cfg.read(cfgpath)

    fname = file.metadata["name"] + ".pro"
    profilename = os.path.expanduser(f"~/data/snowpack/output/{fname}")
    cfg.set("SNOWPRO", "PRO_FILE_PATH", profilename)
    cfg.set("SNOWPRO", "OUTPUT_DIR", outfolder)
    sdate = file.metadata["eventDate"].strftime("%Y-%m-%d")
    cfg.set("SNOWPRO-EVO", "START_DATE", sdate)
    edate = edate.strftime("%Y-%m-%d")
    cfg.set("SNOWPRO-EVO", "END_DATE", edate)
    prof_dt = file.metadata["eventDate"].strftime("%Y-%m-%dT%H:%M")
    cfg.set("SNOWPRO-PROF", "DATETIME", prof_dt)

    return cfg
