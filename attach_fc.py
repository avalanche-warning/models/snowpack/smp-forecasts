################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awio.database
from awsmet import SMETParser
from awsmp.constants import Proc
import nwp_provider.smet_generator as awnwp
from nwp_provider.smet_generator import DataError

import datetime
import gridfs
import os

def _extract_meteo_data(file, outfilename, meteo_source):
    lat = file.metadata["latitude"]
    lon = file.metadata["longitude"]
    prof_dt = file.metadata["eventDate"]
    sdate = prof_dt - datetime.timedelta(days=1) # buffer
    edate = prof_dt + datetime.timedelta(days=7) # forecast # TODO: make span settable

    outpath = os.path.dirname(outfilename)
    os.makedirs(outpath, exist_ok=True)

    success = awnwp.generate_smet_single(meteo_source=meteo_source, out_path=outfilename,
        lat=lat, lon=lon, nearest=True, datetime_start=sdate, datetime_end=edate)

    # we can use full and partial data:
    return success == DataError.OK or success == DataError.TIMESPAN_NA

def attach_fc(datab, fid, meteo_source: str, pnt, reprocess: bool):
    file = datab.gfs.find_one({"_id": fid})
    if file.metadata["proclevel"] >= Proc.METEOFC:
        return True
    fname = file.metadata["name"]

    query = {"metadata.type": "smet", "metadata.pnt": f"{fname}.pnt"}
    ex_smet = datab.gfs.find_one(query)
    if ex_smet:
        if reprocess:
            datab.gfs.delete(ex_smet._id)
        else:
            return False # should not happen due to processing flag

    _meteo_outfilename = os.path.expanduser(f"~smp/data/smet/{fname}.smet")
    success = _extract_meteo_data(file, _meteo_outfilename, meteo_source)
    if not success:
        return False

    # We want to be able to modify HS freely, potentially assimilating
    # more data later on. So, for HS we take the SMP snow height and
    # add PSUM cumulatively:
    smetp = SMETParser(_meteo_outfilename)
    header = smetp.header
    alt = file.metadata["elevation"]
    if not alt:
        alt = 2000 # TODO: get from grid
    header["altitude"] = str(alt)
    df = smetp.df()
    snowh = pnt.samples.distance.iloc[-1]
    cumsum = df["PSUM"].cumsum() # kg/m² ~ mm
    df["HS"] = round((snowh + cumsum) / 1000, 3)
    SMETParser.write_df(df, outfilename=_meteo_outfilename, header=header)

    metadata = {
        "type": "smet",
        "period": smetp.get_timespan(_meteo_outfilename),
        "pnt": file.metadata["name"] + ".pnt",
        "name": file.metadata["name"]
    }
    with open(_meteo_outfilename, "rb") as content:
        datab.gfs.put(content, filename=f"{fname}.smet", metadata=metadata)
    result = datab.db.fs.files.update_one(
        {"_id": fid},
        {"$set": {
            "metadata.proclevel": Proc.METEOFC,
            "metadata.smet": f"{fname}.smet",
        }},
    )
    if result.modified_count != 1:
        return False
    os.remove(_meteo_outfilename) # this is now in the database
    return True
