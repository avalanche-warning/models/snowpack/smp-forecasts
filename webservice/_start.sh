#!/usr/bin/env bash
################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# Helper script to start the SMP webservice manually for output monitoring.
# This file is not in use by the toolchain and makes the service more verbose.

export DASH_URL_BASE_PATHNAME=/upload/
/opt/awsome/venvs/smp/bin/python3 -m gunicorn smp_service:server --reload # reload on file change
