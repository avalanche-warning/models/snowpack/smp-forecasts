################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

from awio import database
from awsmp.constants import Proc
import base64
import dash
import gridfs
from pymongo import MongoClient
import subprocess
import sys

#app = dash.Dash(__name__, requests_pathname_prefix="/smp/smp_service", routes_pathname_prefix="/smp/")
app = dash.Dash(__name__)
app.title = "SMP AWSOME"
server = app.server
_dbclient = MongoClient("localhost", 27017)

_upload_style = {
    "width": "100%", "height": "60px", "lineHeight": "60px",
    "borderWidth": "1px", "borderStyle": "dashed", "borderRadius": "5px",
    "textAlign": "center", "margin": "10px"
}
app.layout = dash.html.Div([
    dash.html.H1("Upload of SnowMicroPen measurements to AWSOME"),
    dash.html.Br(),
    dash.dcc.Input(id="credentials", type="text", placeholder="Credentials"),
    dash.html.Br(),
    dash.dcc.Upload(
        id="upload-pnt",
        children=dash.html.Div(["Drag and drop or select SnowMicroPyn *.pnt file"]),
        style=_upload_style,
        multiple=False
    ),
    dash.dcc.Upload(
        id="upload-caaml",
        children=dash.html.Div(["Drag and drop or select CAAML file"]),
        style=_upload_style,
        multiple=False
    ),
    dash.dcc.Upload(
        id="upload-temperature",
        children=dash.html.Div(["Drag and drop or select temperature profile"]),
        style=_upload_style,
        multiple=False
    ),
    dash.dcc.Upload(
        id="upload-bulk",
        children=dash.html.Div(["Bulk drag and drop or select files"]),
        style=_upload_style,
        multiple=True
    ),
    dash.html.Button("Submit", id="btn-submit", n_clicks=0),
    dash.html.Br(),
    dash.html.Div(id="container-feedback")
])

def smp_upload(pnt_fname, pnt_cont, caaml_fname, caaml_cont,
        temp_fname, temp_cont, credentials):
    if pnt_cont is None:
        return dash.html.P("Error: A pnt file is required.")
    if not pnt_fname.lower().endswith(".pnt"):
        return dash.html.P('Error: Expecting SMP files to have extension ".pnt"')
    db = _dbclient["smp"]
    fs = gridfs.GridFS(db)
    metadata = {
        "type": "pnt",
        "proclevel": Proc.UNPROCESSED,
        "submitter": credentials
    }
    if database.key_exists(fs, pnt_fname):
        return dash.html.P(f'Error: File "{pnt_fname}" already in storage, refusing to overwrite.')

    _, pnt_data = pnt_cont.split(",")
    feedback = ["Submitted:", f"PNT: {pnt_fname}"]
    if caaml_cont is not None and caaml_fname.lower().endswith(".caaml"):
        _, caaml_data = pnt_cont.split(",")
        fs.put(base64.b64decode(caaml_cont), filename=caaml_fname,
            metadata={
                "type": "caaml",
                "pnt": pnt_fname,
            })
        metadata["caaml"] = caaml_fname
        feedback.append(f"CAAML: {caaml_fname}")
    if temp_cont is not None:
        fs.put(base64.b64decode(temp_cont), filename=temp_fname,
            metadata={"type": "temperature", "pnt": pnt_fname})
        metadata["temperature"] = temp_fname
        _, temp_data = pnt_cont.split(",")
        feedback.append(f"Temperature profile: {temp_fname}")

    pnt_bin = base64.b64decode(database._pad_b64_data(pnt_data))
    fs.put(pnt_bin, filename=pnt_fname, metadata=metadata)

    subprocess.call([sys.executable, "/home/smp/smp-chain/run_smp_chain.py"])

    children = []
    for item in feedback:
        children.append(dash.html.P(item))
    return dash.html.Div(children)

@app.callback(
    dash.Output("container-feedback", "children"),
    dash.Input("btn-submit", "n_clicks"),
    dash.State("credentials", "value"),
    dash.State("upload-pnt", "filename"),
    dash.State("upload-pnt", "contents"),
    dash.State("upload-caaml", "filename"),
    dash.State("upload-caaml", "contents"),
    dash.State("upload-temperature", "filename"),
    dash.State("upload-temperature", "contents")
)
def update_output(n_clicks, credentials, pnt_fname, pnt_cont,
        caaml_fname, caaml_cont, temp_fname, temp_cont):
    valid_credentials = [None] # TODO
    
    if n_clicks == 0:
        return dash.html.P("No submission yet.")
    if not credentials in valid_credentials:
        return dash.html.P("Unknown credentials.")
    feedback = smp_upload(pnt_fname, pnt_cont, caaml_fname, caaml_cont,
        temp_fname, temp_cont, credentials)
    return feedback
