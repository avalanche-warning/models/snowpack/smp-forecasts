# This file needs to be in the same folder the server is started from
# or given as command line argument.

bind = "localhost:8090"
workers = 1
timeout = 120
