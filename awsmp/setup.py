#!/usr/bin/env python3

from setuptools import setup

setup(
    name="awsmp",
    version="0.0.1",
    description = "Functionality to link SnowMicroPen measurements to AWSOME",
    url="https://gitlab.com/avalanche-warning/snow-cover/model-chains/smp-chain",
    author="Michael Reisecker",
    author_email="m.r@sbg.at",
    license="GPL3",
    packages=["awsmp"],
)
