from awgen import awtools
from awsmet import SMETParser
import datetime
import os
import subprocess
import sys

def dl_aws_tss(station, timestamp):
    t_buffer = 24
    sdate = timestamp - datetime.timedelta(hours=t_buffer)
    edate = timestamp + datetime.timedelta(hours=t_buffer)
    sdate = sdate.strftime("%Y-%m-%dT%H:%M:%S")
    edate = edate.strftime("%Y-%m-%dT%H:%M:%S")
    timespan = f"{sdate}/{edate}"
    download_script_path = "/opt/awsome/code/data/fetchers/wiski/wiski_rest.py"
    output_dir = os.path.join(os.path.expanduser("~"), "station_data")
    ret = subprocess.call([sys.executable, download_script_path, station, timespan, output_dir])
    if ret != 0:
        return None
    smet = SMETParser(os.path.join(output_dir, f"{station}.smet"))
    return smet.df()

def get_closest_aws_tss(coords):
    ogd_path = os.path.join(os.path.expanduser("~"), "ogd.geojson")
    if not os.path.exists(ogd_path):
        awtools.dl_stations_list(ogd_path)
    stat, dist = awtools.find_closest_station(coords, ogd_path, has_field="OFT")
    return stat, dist
