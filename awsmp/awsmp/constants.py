from enum import IntEnum

class Proc(IntEnum):
    UNPROCESSED = 0
    METADATA = 1
    CAAML = 2
    SNOWT = 3
    METEOFC = 4
    PRO = 5
    PLOT = 6
    ERROR = 9

