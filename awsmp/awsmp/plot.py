import gridfs
import plotly.io as pio
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from pymongo import MongoClient
import snowmicropyn as smp

def plot_all_raw_pnts(outfolder, database_name="smp"):
    pio.templates.default = "plotly_white"
    pio.templates["plotly_white"].layout.font.size = 8

    dbclient = MongoClient("localhost", 27017)
    db = dbclient[database_name]
    gfs = gridfs.GridFS(db)

    query = {"metadata.type": "pnt", "metadata.proclevel":  { "$gte": 5 }}# TODO: semantic value
    for file in gfs.find(query):
        content = file.read()
        pro = smp.Profile.load(content, binary=True)
        plot_raw_smp(pro, file.metadata, outfolder)

def plot_raw_smp(pro, meta, outfolder: str):
    """Plot info coming directly from the pnt files."""
    specs = [ # list of (rows) lists of dictionaries (cols)
        [{"secondary_y": True, "rowspan": 4}],
        [None],
        [None],
        [None],
        [{"secondary_y": True}],
    ]
    fig = make_subplots(
        rows=5,
        specs=specs,
        shared_xaxes=True,
    )

    derivs = {} # dict of derivatives with all SnowMicroPyn parameterizations
    surface = pro.detect_surface()
    ground = pro.detect_ground()

    # pre-calc all derivatives to be able to sort them in the legend:
    for param in smp.params:
        dd = pro.calc_derivatives(parameterization=param, snowpack_only=False,
            hand_hardness=True, optical_thickness=True, names_with_units=False)
        derivs[param] = dd

    # plot the mean force signal of an arbitrary parameterization:
    any_deriv = next(iter(derivs.values()))
    fig.add_trace(
        go.Scatter(
            x=any_deriv["distance"],
            y=any_deriv["force_median"],
            name="Median force of SMP signal",
            mode="lines",
            line_color="#1f77b4", # matplotlib C0
        ), row=1, col=1,
    )

    # plot all density parameterizations:
    for param in derivs:
        fig.add_trace(
            go.Scatter(
                x=derivs[param]["distance"],
                y=derivs[param][f"{param}_density"],
                name="Density " + smp.params[param].name,
                mode="lines",
                line_color=smp.params[param]._density_color,
            ), row=1, col=1, secondary_y=True,
        )

    # plot all specific surface area parameterizations:
    param_with_ssa = None
    for param in derivs:
        if not derivs[param][f"{param}_ssa"].any():
            continue # no SSA parameterization for this author
        fig.add_trace(
            go.Scatter(
                x=derivs[param]["distance"],
                y=derivs[param][f"{param}_ssa"],
                name="SSA " + smp.params[param].name,
                mode="lines",
                line_color=smp.params[param]._ssa_color,
            ), row=1, col=1, secondary_y=True,
        )
        param_with_ssa = param

    # plot surface and ground as vertical lines:
    ymax = any_deriv["force_median"].max()
    fig.add_trace(
        go.Scatter(
            x=[surface, surface],
            y=[0, ymax],
            name="Surface",
            mode="lines",
            line_color="#d62728",
            ), row=1, col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=[ground, ground],
            y=[0, ymax],
            name="Ground",
            mode="lines",
            line_color="#d62728",
            ), row=1, col=1,
    )

    # plot hand hardness index and optical thickness in 2nd figure:
    fig.add_trace(
        go.Scatter(
            x=derivs[param_with_ssa]["distance"],
            y=derivs[param_with_ssa]["hand_hardness"],
            name="Hand hardness " + smp.params[param].name,
            mode="lines",
        ), row=5, col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=derivs[param_with_ssa]["distance"],
            y=derivs[param_with_ssa]["optical_thickness"].apply(lambda xx: xx * 1000), # m -> mm
            name="Optical thickness " + smp.params[param].name,
            mode="lines",
        ), row=5, col=1, secondary_y=True,
    )

    # add captions and legend:
    prof_name = meta["name"]
    prof_date = meta["eventDate"].isoformat()
    fig.update_layout(
        title_text=f'SMP profile "{prof_name}" recorded on {prof_date}',
        title_x=0.5, # center
        xaxis2_title="Depth (mm)",
        yaxis_title="Median force (N)",
        yaxis2_title="Density (kg/m³) and Specific Surface Area (m²/kg)",
        yaxis3_title="Hand hardness index",
        yaxis4_title="Optical thickness (mm)",
    )
    fig.update_layout(
        legend=dict(
            orientation="h",
            yanchor="top",
        )
    )

    html = fig.to_html()
    with open(f"{outfolder}/{prof_name}.html", "w") as figout:
        figout.write(html)
