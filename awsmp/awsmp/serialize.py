import json
import gridfs
from pymongo import MongoClient

# https://admin.avalanche.report/dev/#/modelling/awsome?config=https:%2F%2Fmodels.alpine-software.at%2Fpublic%2Fsmp-chain%2Fawsome_smp.json

def db2geojson(database_name="smp"):
    dbclient = MongoClient("localhost", 27017)
    db = dbclient[database_name]
    gfs = gridfs.GridFS(db)

    geojson = {
        "type": "FeatureCollection",
        "features": []
    }
    query = {"metadata.type": "pnt", "metadata.proclevel": {"$gte": 1}}
    for pnt in gfs.find(query):
        feature = {
            "type": "Feature",
            "properties": {},
            "geometry": {"type": "Point", "coordinates": []}
        }
        coords = [pnt.metadata["longitude"], pnt.metadata["latitude"], pnt.metadata["elevation"]]
        feature["geometry"]["coordinates"] = coords
        feature["properties"]["id"] = pnt.metadata["name"]
        geojson["features"].append(feature)

    return geojson
