import os
import gridfs
from pymongo import MongoClient

class SMPDatabase:
    def __init__(self, database_name="smp"):
        dbclient = MongoClient("localhost", 27017)
        self.db = dbclient[database_name]
        self.gfs = gridfs.GridFS(self.db)

def _get_db(database_name="smp"):
    dbclient = MongoClient("localhost", 27017)
    db = dbclient[database_name]
    return db

def _get_gfs(database_name="smp"):
    db = _get_db(database_name)
    gfs = gridfs.GridFS(db)
    return gfs

def _dump_file(ftype, name, outpath, outfilename=None, database_name: str="smp"):
    gfs = _get_gfs(database_name)
    query = {"metadata.type": ftype, "metadata.name": name}
    file = gfs.find_one(query)
    if not file:
        return False
    content = file.read()
    os.makedirs(outpath, exist_ok=True)
    export_path = file.filename
    if outfilename:
        export_path = outfilename
    with open(os.path.join(outpath, export_path), "wb") as fout:
        fout.write(content)
    return True

def dump_caaml(name, outpath, outfilename=None, database_name: str="smp"):
    return _dump_file("caaml", name, outpath, outfilename, database_name)

def dump_caamls(outpath, database_name: str="smp"):
    os.makedirs(outpath, exist_ok=True)
    gfs = _get_gfs(database_name)
    query = {"metadata.type": "caaml"}
    for file in gfs.find(query):
        content = file.read()
        with open(os.path.join(outpath, file.filename), "wb") as caaml:
            caaml.write(content)
    return True

def dump_smet(name, outpath, outfilename=None, database_name: str="smp"):
    return _dump_file("smet", name, outpath, outfilename, database_name)

def dump_pnt(name, outpath, outfilename=None, database_name: str="smp"):
    return _dump_file("pnt", name, outpath, outfilename, database_name)

def dump_pnts(outpath, database_name: str="smp"):
    os.makedirs(outpath, exist_ok=True)
    gfs = _get_gfs(database_name)
    query = {"metadata.type": "pnt"}
    for file in gfs.find(query):
        pnt = file.read()
        with open(os.path.join(outpath, file.name), "wb") as pntfile:
            pntfile.write(pnt)
        print(f'Dumped "{file.name}"')

def print_file_list(database_name="smp"):
    db = _get_db(database_name)
    for file in db.fs.files.find():
        print(file["filename"], file["metadata"])

def delete_files(filetype, database_name="smp"):
    # TODO: update metadata of connected files?
    gfs = _get_gfs(database_name)
    query = {"metadata.type": filetype}
    dcount = 0
    for file in gfs.find(query):
        gfs.delete(file._id)
        dcount += 1
    print(f"Deleted {dcount} documents of type {filetype}")

def delete_all_files(database_name="smp"):
    gfs = _get_gfs(database_name)
    dcount = 0
    for file in gfs.find({}):
        gfs.delete(file._id)
        dcount += 1
    print(f"Deleted {dcount} documents")
