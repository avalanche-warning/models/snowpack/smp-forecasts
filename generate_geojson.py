#!/usr/bin/env python3
################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awio
import awset
from awsmp.serialize import db2geojson
import json
import os

def generate_geojson(fpublic):
    geojson = db2geojson()
    geojson_obj = json.dumps(geojson, indent=4)
    outfile = os.path.join(fpublic, "public/smp-chain", "smp-profiles.json")
    with open(outfile, "w") as geojson:
        geojson.write(geojson_obj)

def update_dash_config(fpublic):
    outfile = os.path.join(fpublic, "public/smp-chain", "cfg-smp-chain.json")
    with open(os.path.join(awio.get_scriptpath(__file__), "./resources/cfg-smp-chain.template.json"), "r") as json:
        data = json.readlines()
    with open(outfile, "w") as json:
        print("Output to ", outfile)
        for line in data:
            line = line.replace("$AWSOME_HOST", awset.aenv("AWSOME_HOST"))
            json.write(line)
