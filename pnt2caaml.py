################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import warnings # DEBUG - remove this!
warnings.filterwarnings("ignore", category=UserWarning, module=r"(sklearn|scipy)")

import awio
from awsmp.constants import Proc
import awsmp.stations
import awwrf.wrf_extract
import io
import netCDF4
import numpy as np
import os
import pandas as pd
import snowmicropyn as smp
from snowmicropyn.pyngui.document import Document
import xml.etree.ElementTree as ET

_grainshape_training_data = os.path.expanduser("~/snowmicropyn/snowmicropyn/ai/trained_model_rhossa.dat")

def _get_export_settings():
    export_settings = {}
    export_settings["export_grainshape"] = True
    export_settings["use_pretrained_model"] = True
    export_settings["trained_input_path"] = _grainshape_training_data
    export_settings["remove_negative_data"] = True

    export_settings["scaler"] = "standard" # pre-processing: data scaling
    export_settings["model"] = "svc" # use a Support Vector Machine
    export_settings["model_svc_gamma"] = 100

    export_settings["merge_layers"] = True
    export_settings["similarity_percent"] = 200
    export_settings["discard_thin_layers"] = True
    export_settings["discard_layer_thickness"] = 1
    return export_settings

def pnt2caaml(datab, fid, pnt):
    doc = Document(pnt)
    file = datab.gfs.find_one({"_id": fid})
    if file.metadata["proclevel"] >= Proc.CAAML:
        return True

    caaml = doc.export_caaml(None, export_settings=_get_export_settings(), binary=True) # it's a tree
    xml = ET.tostring(caaml.getroot(), encoding="utf-8", method="xml")

    xml = resample_density(xml)

    xml_bytes = io.BytesIO(xml) # put as bytes for consistency

    caaml_name = doc._profile.name + ".caaml"
    for old in datab.gfs.find({"metadata.type": "caaml", "filename": caaml_name}):
        datab.gfs.delete(old._id)
    metadata = {
        "type": "caaml",
        "pnt": file.filename,
        "name": doc._profile.name
    }
    datab.gfs.put(xml_bytes, filename=caaml_name, metadata=metadata)
    result = datab.db.fs.files.update_one(
        {"_id": fid},
        {"$set": {
            "metadata.proclevel": Proc.CAAML,
            "metadata.caaml": caaml_name,
        }},
    )
    if result.modified_count != 1:
        return False
    return True

def resample_density(xml):
    """
    The SNOWPACK-checks are in CaamlIO::DataResampler::checkIfDataIsValid()
    """
    _ns = {"caaml": "http://caaml.org/Schemas/SnowProfileIACS/v6.0.3", "gml": "http://www.opengis.net/gml"}
    for key, val in _ns.items():
        ET.register_namespace(key, val)
    root = ET.fromstring(xml)

    densityroot = root.find("caaml:snowProfileResultsOf/caaml:SnowProfileMeasurements/caaml:densityProfile", _ns)
    strat_layers = root.findall("caaml:snowProfileResultsOf/caaml:SnowProfileMeasurements/caaml:stratProfile/caaml:Layer", _ns)
    density_layers = densityroot.findall("caaml:Layer", _ns)

    depths = [None] * len(density_layers)
    densities = [None] * len(density_layers)

    rowcount = 0
    for dlayer in density_layers:
        depth = float(dlayer.find("caaml:depthTop", _ns).text)
        density = float(dlayer.find("caaml:density", _ns).text)
        depths[rowcount] = depth
        densities[rowcount] = density
        rowcount += 1
    density_df = pd.DataFrame(list(zip(depths, densities)), columns = ["depth", "density"])

    # sample as many density layers as there are strat layers:
    matched_idx = np.linspace(0, len(density_layers) - 1, len(strat_layers), dtype=int)
    sampled_density_df = density_df.iloc[matched_idx].copy()

    # stretch the layer depths of density recording to layer depth of stratigraphy profile:
    sfactor = float(strat_layers[-1].find("caaml:depthTop", _ns).text) # last recorded depth in stratigraphy
    sfactor = sfactor / sampled_density_df.iloc[-1]["depth"]
    sampled_density_df["depth"] = sampled_density_df["depth"].apply(lambda xx: xx * sfactor)

    # re-add density profile:
    for node in density_layers:
        densityroot.remove(node)

    for _, row in sampled_density_df.iterrows():
        layer = ET.SubElement(densityroot, "caaml:Layer")
        depth_top = ET.SubElement(layer, "caaml:depthTop")
        depth_top.set("uom", "cm")
        depth_top.text = str(row["depth"])
        density = ET.SubElement(layer, "caaml:density")
        density.set("uom", "kgm-3")
        density.text = str(row["density"])

    xml = ET.tostring(root, encoding="utf-8")
    return xml
