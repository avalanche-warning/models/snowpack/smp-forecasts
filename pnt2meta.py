################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

from awsmp.constants import Proc
import os

def pnt2meta(datab, fid, pnt):
    file = datab.gfs.find_one({"_id": fid})
    if file.metadata["proclevel"] >= Proc.METADATA:
        return True

    pname = os.path.splitext(pnt.name)[0] # no file suffix (if there's one)
    pnt_meta = {
        "metadata.name": pname,
        "metadata.eventDate": pnt.timestamp, # CAAML format names
        "metadata.latitude": pnt.coordinates[0],
        "metadata.longitude": pnt.coordinates[1],
        "metadata.location": pnt.coordinates[::-1], # SMP is lat/lon, GeoJson Point is lon/lat
        "metadata.elevation": pnt.altitude / 100, # cm -> m
        "metadata.smp_serial": pnt.smp_serial,
        "metadata.smp_firmware": pnt.smp_firmware,
        "metadata.proclevel": Proc.METADATA,
    }
    result = datab.db.fs.files.update_one(
        {"_id": fid},
        {"$set": pnt_meta},
    )
    if result.modified_count != 1:
        return False
    return True
