################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awsmp
from awsmp.constants import Proc
import awwrf
import io
import numpy as np
import xml.etree.ElementTree as ET

def assimilate_temps(datab, fid, pnt, max_aws_distance=10):
    """max_aws_distance: km"""
    file = datab.gfs.find_one({"_id": fid})
    if file.metadata["proclevel"] >= Proc.SNOWT:
        return True
    location = file.metadata["location"]
    station, dist = awsmp.stations.get_closest_aws_tss(location)
    dist = round(dist / 1000, 2)
    flag_dist = False
    if station and dist <= max_aws_distance:
        flag_dist = True
        stat_id = station["properties"]["LWD-Nummer"]
        print(f'[i] Closest TSS station is "{stat_id}" ({dist} km away)')
        #df = awsmp.stations.dl_aws_tss(stat_id, pnt.timestamp)
        df = None
    if not flag_dist or not df:
        temps, alt = get_nwp_data(datab, fid, pnt)
        if temps is None:
            return False
        success = attach_temp_nwp(datab, fid, pnt, temps, alt)

    result = datab.db.fs.files.update_one(
        {"_id": fid},
        {"$set": {"metadata.proclevel": Proc.SNOWT}},
    )
    if result.modified_count != 1:
        return False
    return success

def get_nwp_data(datab, fid, pnt):
    #TSNO, ZSNSO, SNICE, SNLIQ
    #https://github.com/NCAR/noahmp/blob/981d4f859ce6c64213d38a783654c05b47b3485e/src/module_sf_noahmplsm.F#L5251
    file = datab.gfs.find_one({"_id": fid})
    request_date = pnt.timestamp
    lonlat = file.metadata["location"]
    tt, alt = _extract_wrf_data("nordkette", request_date, lonlat)
    if tt is None:
        return None, None
    max_nwp_depth = tt[-1, 0]
    # TODO: dectect ground, surface
    max_smp_depth = pnt.samples.distance.iloc[-1] # mm
    stretch = max_smp_depth / tt[0, -1]
    tt_stretch = tt
    tt_stretch[:, 0] = tt_stretch[:, 0] * max_smp_depth / max_nwp_depth / 10
    tt_stretch[:, 1] = tt_stretch[:, 1] - 273.15
    # TODO: ground to 0?
    return tt_stretch, alt

def attach_temp_aws():
    pass

def attach_temp_nwp(datab, fid, pnt, temps, altitude, reprocess=False):
    file = datab.gfs.find_one({"_id": fid})
    _ns = {"caaml": "http://caaml.org/Schemas/SnowProfileIACS/v6.0.3", "gml": "http://www.opengis.net/gml"}
    for key, val in _ns.items():
        ET.register_namespace(key, val)
    caaml = file.metadata["caaml"]
    caaml = datab.gfs.find_one({"filename": caaml})
    root = ET.fromstring(caaml.read())

    tparent = root.find("caaml:snowProfileResultsOf/caaml:SnowProfileMeasurements", _ns)
    tprof = tparent.find("caaml:tempProfile", _ns)
    if tprof:
        if reprocess:
            tparent.remove(tprof)
        else:
            return False # should not happen due to processing flag
    cparent = f"caaml:metaData/caaml:comment"
    cnode = root.find(cparent, _ns)
    comment_text = " Surface- and snow-temperatures taken from numerical weather model."
    if comment_text not in cnode.text: # not left over from previous processing
        cnode.text = cnode.text + " Surface- and snow-temperatures taken from numerical weather model."

    tparent = "caaml:snowProfileResultsOf/caaml:SnowProfileMeasurements"
    tnode = root.find(tparent, _ns)
    if not tprof:
        tprof = ET.SubElement(tnode, "caaml:tempProfile")
    tmeta = ET.SubElement(tprof, "caaml:tempMetaData")

    for row in temps:
        tobs = ET.SubElement(tprof, "caaml:Obs")
        tdepth = ET.SubElement(tobs, "caaml:depth")
        tdepth.set("uom", "cm")
        tdepth.text = str(abs(round(row[0], 3)))
        ttemp = ET.SubElement(tobs, "caaml:snowTemp")
        ttemp.set("uom", "degC")
        ttemp.text = str(round(row[1], 3))

    loc_ref = root.find("caaml:locRef", _ns)
    valid_ele = loc_ref.find("caaml:validElevation", _ns)
    if not valid_ele:
        valid_ele = ET.SubElement(loc_ref, "caaml:validElevation")
        ele = ET.SubElement(valid_ele, "caaml:ElevationPosition")
        ele.set("uom", "m")
        position = ET.SubElement(ele, f"caaml:position")
        position.text = str(alt)

    datab.gfs.delete(caaml._id)
    ET.indent(root, space="\t", level=0)
    xml_bytes = ET.tostring(root, encoding="utf-8", method="xml", xml_declaration=True)
    xml_bytes = io.BytesIO(xml_bytes) # put as bytes for consistency
    datab.gfs.put(xml_bytes, filename=caaml.filename, metadata=caaml.metadata)

    return True

def _extract_wrf_data(domain: str, request_date, lonlat, wrf_folders=["/mnt/wrf"]):
    var = awwrf.wrf_extract.wrf_extract(
        wrf_folders, lonlat[1], lonlat[0], ["TSNO", "ZSNSO", "TSK"],
        domain, request_date)
    if var is None:
        return None, None
    # snow temperature:
    var["TSNO"].values[var["TSNO"].values == 0] = np.nan
    # snow and soil layer depths (bottom of layers):
    n_snowlayers = len(var["TSNO"]["snow_layers_stag"].values)
    layer_locs_snow = var["ZSNSO"].isel(snso_layers_stag=slice(0, n_snowlayers))
    layer_locs_snow.values[layer_locs_snow.values == 0] = np.nan
    tt = np.stack([layer_locs_snow.values, var["TSNO"].values], axis=1)
    if tt[0, 0] == 0: # disregard depth=0 if available in TSNO - use surface T
        tt = tt[1:, :]
    surf = [0, var["TSK"].values.item()]
    tt = np.vstack((surf, tt))
    tt = tt[~np.isnan(tt).any(axis=1)] # drop NAs

    alt = awwrf.wrf_extract.wrf_extract(
        wrf_folders, lonlat[1], lonlat[0], ["ter"],
        domain, request_date)
    alt = round(float(alt["ter"].values), 1)
    return tt, alt
