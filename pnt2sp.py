################################################################################
# Copyright 2024 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

from awsmp.constants import Proc
import awsmp.database
import profsnow
from profsnow.snowpack import PRFEngine

import datetime
import os
import sys

def _dump_files(datab, fid):
    file = datab.gfs.find_one({"_id": fid})
    sp_folder_snow = os.path.expanduser("~/data/snowpack/snow")
    os.makedirs(sp_folder_snow, exist_ok=True)
    sp_folder_meteo = os.path.expanduser("~/data/snowpack/meteo")
    os.makedirs(sp_folder_meteo, exist_ok=True)

    awsmp.database.dump_caaml(file.metadata["name"], sp_folder_snow)
    awsmp.database.dump_smet(file.metadata["name"], sp_folder_meteo)

def start_engine(datab, fid):
    file = datab.gfs.find_one({"_id": fid})
    statid = file.metadata["name"]
    stations = [statid]
    sdate = file.metadata["eventDate"]

    query = {"metadata.type": "smet", "metadata.name": statid}
    smetfile = datab.gfs.find_one(query)
    edate = smetfile.metadata["period"][1]

    smetpath = os.path.expanduser("~/data/snowpack/meteo")
    domain = "nordkette" # TODO: link into real domains
    propath = "/var/www/html/public/smp-cain/pro"
    success, snp = PRFEngine.caaml_forecast(
        smetpath, domain=domain, stations=stations, station_id=statid, sdate=sdate, edate=edate,
        pro_outpath=propath)
    if not success:
        print(snp._stderr)
        sys.exit("Error")

    print("[i] SNOWPACK engine done.")

    result = datab.db.fs.files.update_one(
        {"_id": fid},
        {"$set": {
            "metadata.proclevel": Proc.PRO,
        }},
    )
    if result.modified_count != 1:
        return False
    return True

def pnt2sp(datab, fid):
    file = datab.gfs.find_one({"_id": fid})
    if file.metadata["proclevel"] >= Proc.PRO:
        return True

    _dump_files(datab, fid) # dump files from db to fs for SNOWPACK
    # TODO: delete after
    success = start_engine(datab, fid)

    return success
